import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Arrays;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * A SCRAM code assembler. Hamima Halim1, hhalim1 Evelyn Yeh, eyeh8
 * 
 * @author hamima
 *
 */
public final class SAS {
    /**
     * The input scanner.
     */
    private static Scanner input;

    /**
     * Maximum instructions in a program.
     */
    private static final int INSTR_MAX = 16;

    /**
     * Maximum size of a non-DAT address--4 bits.
     */
    private static final byte ADDRESS_MAX = 0xf;

    /**
     * Maximum size of a DAT variable--8 bits.
     */
    private static final int VARIABLE_MAX = 0xff;

    /**
     * A list of the current labels and associated numbers.
     */
    private static HashMap<String, Integer> labels;
    
    /**
     * The array which stores instructions on the initial pass. If action on
     * that line does not require an address argument (i.e., HLT, labels, etc)
     * then can be empty. instructions[line_num_address][output address]
     */
    private static String[][] instructions = new String[INSTR_MAX][2 + 1];
    
    /**
     * In instructions array, instruction column.
     */
    private static final int INSTRUCTION = 0;

    /**
     * In instructions array, address column.
     */
    private static final int ADDRESS = 1;
    
    /**
     * In instructions array, denotes line number in source for error logging
     * in finalRead().
     */
    private static final int LINE = 2;
    
    /**
     * For byte shifting.
     */
    private static final int FOUR = 4;
    
    /**
     * Number of errors encountered.
     */
    private static int errors = 0;
    
    /**
     * Number of instructions in microprogram--must be less than 16.
     */
    private static int size = 0;
    
    /**
     * Output stream.
     */
    private static FileOutputStream outputStream;

    /**
     * Halt.
     */
    private static final int HLT = 0x0;
    /**
     * Load from accumulator.
     */
    private static final int LDA = 0x1;
    /**
     * Indirect load.
     */
    private static final int LDI = 0x2;
    /**
     * Store to accumulator.
     */
    private static final int STA = 0x3;
    /**
     * Store indirectly.
     */
    private static final int STI = 0x4;
    /**
     * Add (with carry).
     */
    private static final int ADD = 0x5;
    /**
     * Subtract (with borrow).
     */
    private static final int SUB = 0x6;
    /**
     * Jump to program counter step.
     */
    private static final int JMP = 0x7;
    /**
     * Jump to program counter step is zero flag set.
     */
    private static final int JMZ = 0x8;

    /**
     * Constructor.
     */
    private SAS() {
    }

    /**
     * Reads a line.
     * 
     */
    private static void firstRead() {
        String[] currsplit;
        byte address = 0x0;
        int line = 1;
        while (input.hasNextLine()) {
            if (address >= INSTR_MAX) {
                System.err.print("Err at line " 
                        + line + ": more instructions than 6");
                errors++;
            }

            currsplit = input.nextLine().trim().split("\\s+");
            // processes lines based on first token
            if (currsplit.length <= 1 && currsplit[0].equals("")) {
                line++;
                continue;
                
            } else if (isComment(currsplit[0])) {
                line++;
                continue;
                
            } else if (isLabel(currsplit[0])) {
                parseLabel(currsplit, line, address);
                if (currsplit.length > 1 && isOpCode(currsplit[1])) {
                    address++;
                }

            } else if (isOpCode(currsplit[0])) {
                parseOpCode(currsplit, line, address);
                address++;

            } else {
                System.err.println("Error at line " + line 
                        + ": first token " + currsplit[0] + " is not an " 
                        + "operation, label or comment.");
                errors++;
            }
            line++;
        }
    }

    /**
     * Completes second read of assembler-- identifies label addresses, checks
     * addresses and contents for correctness.
     * @param filename output filename
     * @throws IOException hoo
     */
    private static void finalRead(String filename) throws IOException {
        String op = "";
        String line = "";
        Integer[] addresses = new Integer[size];
        
        for (int i = 0; i < size; i++) {
            op = instructions[i][INSTRUCTION];
            line = instructions[i][LINE];

            try {
                addresses[i] = Integer.parseInt(instructions[i][ADDRESS]);
                
            } catch (NumberFormatException e) {
                if (!op.equals("hlt") 
                        && labels.get(instructions[i][ADDRESS]) == null) {
                    System.err.println("Error at line " 
                        + line + ": unspecified label " 
                            + instructions[i][ADDRESS]);
                    errors++;
                    continue;
                } else {
                    addresses[i] = labels.get(instructions[i][ADDRESS]);
                }
            }

            if (op.equals("dat") && addresses[i] > VARIABLE_MAX) {
                System.err.println("Err at line " 
                        + line + ": data variable greater than 8 bytes.");
                errors++;
                continue;
            }
            if (!op.equals("dat") && addresses[i] > ADDRESS_MAX) {
                System.err.println("Err at line " 
                        + line + ": address greater than 4 bytes.");
                errors++;
                continue;
            }
        }
        
        writeToFile(filename, addresses);
    }
    
    /**
     * Outputs bytes to file if we can.
     * @param filename output filename
     * @param addresses array of addresses
     * @throws IOException no output
     */
    private static void writeToFile(String filename, Integer[] addresses) 
            throws IOException {
        if (errors == 0) {
            String op;
            for (int i = 0; i < size; i++) {
                op = instructions[i][INSTRUCTION];
                if (!op.equals("dat")) {
                    outputStream.write((byte) ((getOpCode(op).byteValue() 
                            << FOUR) | addresses[i].byteValue()));
                } else {
                    outputStream.write(addresses[i].byteValue());
                }
            }
            outputStream.close();
        } else {
            System.out.println("Found " + errors + " errors.");
        }
    }

    /**
     * Turns an byte code of operation.
     * 
     * @param op
     *            op code
     * @return byte code
     * @SuppressWarnings("checkstyle:methodlength")
     */
    private static Integer getOpCode(String op) {
        switch (op) {
            case "hlt":
                return HLT;
            case "lda":
                return LDA;
            case "ldi":
                return LDI;
            case "sta":
                return STA;
            case "sti":
                return STI;
            case "add":
                return ADD;
            case "sub":
                return SUB;
            case "jmp":
                return JMP;
            default: // jmz
                return JMZ;
        }
    }

    /**
     * Parses a line that begins with a label.
     * 
     * @param currline
     *            current line
     * @param line
     *            line in instructions
     * @param a the address of the line
     */
    private static void parseLabel(String[] currline, int line, int a) {
        if (currline[0].length() == 1) {
            System.err.println("Err at line " + line + ": empty label");
            errors++;
        }
        String label = currline[0].substring(0, currline[0].length() - 1);
        if (labels.get(label) != null) {
            System.err.println("Err at line " 
                    + line + ": redefined label " + label);
            errors++;
        } 
        labels.put(label, a);
        
        if (currline.length > 1 && isOpCode(currline[1])) {
            parseOpCode(Arrays
                    .copyOfRange(currline, 1, currline.length), line, a);
        }
    }

    /**
     * Parses a line that begins with an opcode. Following token can be an
     * integer or a label. If HLT, address not required. If DAT, address may be
     * 8 bit. Else must have label or 4 bit address
     * 
     * @param currline
     *            current line
     * @param line
     *            line in instructions
     * @param a
     *            the address of the line
     */
    private static void parseOpCode(String[] currline, int line, int a) {
        instructions[a][INSTRUCTION] = currline[0].toLowerCase();

        if (!currline[0].toLowerCase().equals("hlt")) {
            if (currline.length == 1) {
                System.err.println("Err at line " + line + ": instruction " 
                        + currline[0].toLowerCase() + "without address.");
                errors++;
            } else if (currline[1].charAt(0) == '#') {
                System.err.println("Err at line " + line + ": instruction " 
                        + currline[0].toLowerCase() + "without address.");
                errors++;
            }
        } else {
            // halt has no address argument to process.
            return;
        }
        
        size++;
        instructions[a][ADDRESS] = currline[1];
        instructions[a][LINE] = "" + line;
        return;
    }

    /**
     * Checks if current token is a label.
     * @param in
     *            current token
     * @return if current token end with a :.
     */
    private static boolean isLabel(String in) {
        //System.out.println(in);
        return in.charAt(in.length() - 1) == ':';
    }

    /**
     * Checks if current token begins a comment.
     * 
     * @param in
     *            current token
     * @return if current token begins with a #.s
     */
    private static boolean isComment(String in) {
        return in.charAt(0) == '#';
    }

    /**
     * Checks if current token is an opcode.
     * 
     * @param in
     *            current token
     * @return if argument is an opcode
     */
    private static boolean isOpCode(String in) {
        in = in.toLowerCase();
        switch (in) {
            case "hlt":
                return true;
            case "lda":
                return true;
            case "ldi":
                return true;
            case "sta":
                return true;
            case "sti":
                return true;
            case "add":
                return true;
            case "sub":
                return true;
            case "jmp":
                return true;
            case "jmz":
                return true;
            case "dat":
                return true;
            default:
                return false;
        }
    }
    

    /**
     * Main.
     * 
     * @param arg
     *            Argument .s file.
     * @throws IOException
     *             from writing to file.
     */
    public static void main(String[] arg) throws IOException {
        String filename = "";
        String argv = ""; 
        
        //TODO MAKE THIS STANDARD INPUT
        /**if (argv == null) {
            System.err.print("Error: Need file input.");
            System.exit(0);
        } else {
            filename = argv.substring(0, argv.indexOf('.')) + ".scram";
        }**/

        
        input = new Scanner(System.in);

        outputStream = new FileOutputStream("out.scram");
        labels = new HashMap<String, Integer>();
        firstRead(); 
        finalRead(filename);
    }
}